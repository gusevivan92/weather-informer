import axios from "axios";
import { rusWeekDays } from "../catalogs/DaysOfWeek";
import { months } from "../catalogs/MonthsOfYear";

const weatherDailyTemp = document.querySelectorAll(".weather-temp");
const weatherDailyDesc = document.querySelectorAll(".weather-description");
const weatherDailyIcon = document.querySelectorAll(".weather-state");
const weatherWeekDay = document.querySelectorAll(".weather-weekday");
const weatherMonth = document.querySelectorAll(".weather-month");

export function weatherDaily(lat, lng) {
  const response = axios
    .get(
      `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lng}&lang=ru&exclude=current,minutely,hourly,alerts&appid=8593bffd6b1b6e53d09c865d2b7f7165&units=metric`
    )
    .then((res) => {
      let weatherData = res.data;
      weatherDailyTemp.forEach((x, index) => {
        let temp = weatherData.daily[index].temp;
        let tempDay = `${Math.round(temp.day)}&deg ${Math.round(
          temp.night
        )}&deg`;
        x.innerHTML = tempDay;
      });

      weatherDailyDesc.forEach((a, index) => {
        let desc = weatherData.daily[index].weather[0];
        let dayDesc = desc.description;
        a.innerHTML = dayDesc;
      });

      weatherDailyIcon.forEach((i, index) => {
        let getIcon = weatherData.daily[index].weather[0];
        let icon = `<img src="https://openweathermap.org/img/wn/${getIcon.icon}@2x.png">`;
        i.innerHTML = icon;
      });

      weatherWeekDay.forEach((y, index) => {
        let day = weatherData.daily[index];
        let weekday = rusWeekDays()[new Date(day.dt * 1000).getDay()];

        y.innerHTML = `${weekday}`;
        if (y.innerHTML === rusWeekDays()[new Date().getDay()]) {
          y.innerHTML = "Сегодня";
        }
      });

      // let i = 0;
      // weatherMonth.forEach((month) => {
      //   i = i + 1;
      //   month.innerHTML =
      //     `${new Date().getDate()+i} ` +
      //     ` ${months()[new Date().getMonth()]}`;
      // });
    });
}
