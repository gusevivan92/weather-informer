const rootElement = document.createElement("div");

let numOfDays = 7;

class AboveAll {
  constructor(target) {
    this.target = target;
    this.target.className = "container";
    this.init();
  }
  init() {
    // this.target.innerHTML = this.template();
    document.body.prepend(this.target);
  }
}

class AddInput {
  constructor() {
    this.createDiv();
    this.element.className = "input";
    this.createInp();
    this.element.input.name = "city";
    this.element.input.type = "text";
    this.element.input.value = "Moscow";
  }
  createDiv() {
    this.element = document.createElement("div");
    rootElement.appendChild(this.element);
  }
  createInp() {
    this.element.input = document.createElement("input");
    this.element.appendChild(this.element.input);
  }
}

class AddDivItem {
  constructor() {
    for (let i = 0; i < numOfDays; i++) {
      this.create();
      this.element.className = "weather-item";
      if (i % 2 == 0) {
        this.element.className = "weather-item weather-color";
      }
    }
  }
  create() {
    this.element = document.createElement("div");
    rootElement.appendChild(this.element);
  }
}

class AddDivInItem {
  constructor() {
    this.create();
  }
  create() {
    let items = document.querySelectorAll(".weather-item");
    items.forEach((x) => {
      x.innerHTML = `<div class="weather-state"></div>
      <div class="weather-info">
        <div class="weather-weekday"></div>
        <div class="weather-month"></div>
        <div class="weather-description"></div>
      </div>
      <div class="weather-temp">
        <div class="weather-temp-day"></div>
        <div class="weather-temp-night"></div>
      </div>`;
    });
  }
}

// console.log("DOM fully loaded and parsed");
new AboveAll(rootElement);
new AddInput();
new AddDivItem();
new AddDivInItem();
