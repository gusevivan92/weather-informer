import { inputCity } from "./inputCity";
import { cities } from "../catalogs/allCities";

const input = document.querySelector("input[name=city]");

class AddList {
  constructor(inp, arr) {
    this.input = inp;
    this.inputValue = inp.value;
    this.citiArr = arr;
    this.create();
    this.click();
  }
  create() {
    const inputParent = this.input.parentNode;

    this.cityBox = document.createElement("div");
    this.cityBox.className = "cityBox";

    inputParent.appendChild(this.cityBox);
    inputParent.replaceChild(this.cityBox, inputParent.childNodes[1]);

    for (let i = 0; i < this.citiArr.length; i++) {
      this.cityValue = document.createElement("div");

      if (
        this.citiArr[i].city
          .toLowerCase()
          .includes(this.inputValue.toLowerCase())
      ) {
        this.cityValue.innerHTML = this.citiArr[i].city;
        // this.cityValue.tabIndex = i;
        this.cityValue.className = "cityName";
      }

      this.cityBox.appendChild(this.cityValue);
    }

    if (this.cityValue.innerHTML == "") {
      this.cityValue.remove();
    }
  }

  click() {
    document.querySelectorAll(".cityName").forEach((city) => {
      city.addEventListener("click", () => {
       input.value = city.innerHTML
       
      });
    });
  }
}

input.addEventListener("keyup", () => {
  new AddList(input, cities)

  let inputOnClick = document.querySelectorAll('.cityName')
  console.log(inputOnClick)

  inputOnClick.forEach( div => {
    div.addEventListener("click",()=>{
      inputCity(input.value)
    })
  })

});




function defaultCity() {
  inputCity("Moscow");
}

document.addEventListener("DOMcontentloaded", defaultCity());
