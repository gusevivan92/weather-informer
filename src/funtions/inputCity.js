import axios from "axios";

import { weatherDaily } from "./weatherDaily";

export function inputCity(city) {
  const res = axios
    .get(
      `http://www.mapquestapi.com/geocoding/v1/address?key=XGnOuYyPrZLGIwptFo5Gz0SPhG3yOZ40&location=${city}`
    )
    .then((res) => {
      const lat = res.data.results[0].locations[0].latLng.lat;
      const lng = res.data.results[0].locations[0].latLng.lng;
      return [lat, lng];
    })
    .then((res) => weatherDaily(res[0], res[1]));
}
